[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date ('22) | Host          | Presenter 1     | Presenter 2     | Presenter 3      | Presenter 4  |
| ---------- | ------------- | --------------- | --------------- | ---------------- | ------------ |
| 7/20       | Jacki         | Austin Regnery  | Veethika Mishra  | Dan Mizzi-Harris | Jeremy Elder |
| 8/3        | Justin        | Matt Nearents   | Amelia Bauerly  | Daniel Mora      | X            |
| 8/17       | Taurie        | Alexis Ginsberg | Emily Sybrant   | Matej Latin      | X            |
| 8/31       | (APAC) | Katie Macoy     | Michael Le      |                  | X            |
| 9/14       | Marcel        | Nick Brandt     | Emily Bauman    | Nick Leonard     | X            |
| 9/28       | Rayana        | Sunjung Park    | Michael Fangman | Camellia X Yang   | X            |
| 10/12      | Blair         | Matt Nearents   | Becka Lippert   | Philip Joyce     | X            |
| 10/26      | Jacki         | Alexis Ginsberg | Libor Vanc      | Andy Volpe       | X            |
| 11/9       | Justin        | Matej Latin     | Kevin Comoli    | Ali Ndlovu       | X            |
| 11/23/2022 | Taurie        |                 |                 |                  | X            |
| 12/7       | APAC (Rayana) |                 |                 |                  | X            |
| 12/21      | Marcel        |                 |                 |                  | X            |